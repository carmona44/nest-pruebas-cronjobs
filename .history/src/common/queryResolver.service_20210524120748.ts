import { Injectable, ExecutionContext } from "@nestjs/common";
import { I18nResolverOptions, I18nResolver } from "nestjs-i18n";
import { COMMON } from "./constants";

@Injectable()
export class QueryResolverLang implements I18nResolver {
  constructor(@I18nResolverOptions() private keys: string[]) {}

  resolve(context: ExecutionContext) {
    let req: any;

    switch (context.getType() as string) {
      case 'http':
        req = context.switchToHttp().getRequest();
        break;
    }

    let lang: string;

    if (req && req.query) { lang = this.includeLang(req.query.lang); }
    if (req && req.headers) { lang = this.includeLang(req.headers.lang, lang); }

    return lang ? lang : 'en';
  }

  includeLang(lang: string, candidate?: string) {
    if (COMMON.LANGUAGES.includes(lang)) {
        lang = lang;
    } else {
        lang = candidate ? candidate : 'en';
    }
    return lang;
  }
}