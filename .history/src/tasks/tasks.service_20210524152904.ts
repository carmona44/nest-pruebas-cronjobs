import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { PruebasService } from 'src/pruebas/pruebas.service';

@Injectable()
export class TasksService {

    private readonly logger = new Logger(TasksService.name);

    constructor(
        private readonly pruebasService: PruebasService,
        private readonly schedulerRegistry: SchedulerRegistry,
    ){}

    @Cron(CronExpression.EVERY_SECOND)
    handleCron(query) {
        const callback = () => {
            this.logger.warn(`Interval ${query} executing at time (${1000})!`);
        };
    
        const interval = setInterval(callback, 1000);
        this.schedulerRegistry.addInterval(query, interval);
    }
}
