import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PruebasService } from 'src/pruebas/pruebas.service';

@Injectable()
export class TasksService {

    constructor(
        //private readonly pruebasService: PruebasService
    ) {}

    @Cron(CronExpression.EVERY_5_SECONDS)
    handleCron() {
        console.log('escribiendo...');
    }
}
