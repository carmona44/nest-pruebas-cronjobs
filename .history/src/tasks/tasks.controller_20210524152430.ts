import { Controller, Get, Headers } from '@nestjs/common';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {

    constructor(
        private readonly tasksService: TasksService
    ) {}

    @Get()
    public prueba(@Headers('lang') lang) {
        return this.tasksService.handleCron();
    }

}
