import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PruebasService } from 'src/pruebas/pruebas.service';

@Injectable()
export class TasksService {

    private readonly logger = new Logger(TasksService.name);

    constructor(
        private readonly pruebasService: PruebasService
    ){}

    @Cron(CronExpression.EVERY_SECOND)
    handleCron() {
        this.logger.debug('Called every second');
    }
}
