import { Injectable, Scope } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PruebasService } from 'src/pruebas/pruebas.service';

@Injectable({ scope: Scope.DEFAULT })
export class TasksService {

    constructor(
        private readonly pruebasService: PruebasService
    ) {}

    @Cron(CronExpression.EVERY_SECOND)
    handleCron() {
        console.log('escribiendo...');
    }
}
