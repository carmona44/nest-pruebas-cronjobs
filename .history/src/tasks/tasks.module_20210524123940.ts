import { Module } from '@nestjs/common';
import { PruebasModule } from 'src/pruebas/pruebas.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [PruebasModule],
  providers: [TasksService]
})
export class TasksModule {}
