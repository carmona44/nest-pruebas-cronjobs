import { Injectable, Scope } from '@nestjs/common';
import { I18nRequestScopeService } from 'nestjs-i18n';

@Injectable({ scope: Scope.DEFAULT })
export class PruebasService {
    constructor (
        private readonly i18n: I18nRequestScopeService
    ) {}

    public async prueba(lang: string) {
        return await this.i18n.translate('prueba.PRUEBA');
    }
}
