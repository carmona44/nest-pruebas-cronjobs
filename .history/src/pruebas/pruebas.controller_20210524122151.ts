import { Controller, Get, Headers } from '@nestjs/common';
import { PruebasService } from './pruebas.service';

@Controller('pruebas')
export class PruebasController {

    constructor(
        private readonly pruebasService: PruebasService
    ) {}

    @Get()
    public prueba(@Headers('lang') lang) {
        return this.pruebasService.prueba(lang);
    }
}
