import { Injectable } from '@nestjs/common';
import { I18nRequestScopeService, I18nService } from 'nestjs-i18n';

@Injectable()
export class PruebasService {
    constructor (
        //private readonly i18n: I18nService,
        private readonly i18n: I18nRequestScopeService
    ) {}

    public async prueba(lang: string) {
        //return await this.i18n.translate('prueba.PRUEBA', { lang });
        return await this.i18n.translate('prueba.PRUEBA');
    }
}
