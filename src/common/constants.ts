export const COMMON = {
    LANGUAGES: ['es', 'en'],
    LANG: `lang`
}

export const HEADERS = {
    LANG: `lang`,
};

export const LANGUAGES = {
    ES: `es`,
    EN: `en`,
};

export const ENVIRONMENTS = {
    PRODUCTION: "production",
    DEV: "dev",
    LOCAL: "local"
};

export const DTO_MESSAGES = {
    IsNotEmpty: JSON.stringify({key: 'formularyDto.IsNotEmpty', args: {property: '$property'}}),
    IsString: JSON.stringify({key: 'formularyDto.IsString', args: {property: '$property'}}),
    IsTimestamp: JSON.stringify({key: 'formularyDto.IsTimestamp', args: {property: '$property'}}),
    IsNumberString: JSON.stringify({key: 'formularyDto.IsNumberString', args: {property: '$property'}})
}

export const AWSPATH = {
    FISHINGPHOTOS: `fishingPhotos`,
    SPECIESPHOTOS: `common/species`,
    BAITSPHOTOS: `fishingKits/baits`,
    LURESPHOTOS: `fishingKits/lures`,
    RODSPHOTOS: `fishingKits/rods`,
    REELSPHOTOS: `fishingKits/reels`,
    THREADSPHOTOS: `fishingKits/threads`,
    LEADERSPHOTOS: `fishingKits/leaders`,
    FISHINGVIDEOSCATCH: `fishingVideos/catches`,
    FISHINGVIDEOSCATCHTHUMB: `fishingVideos/catches/thumbs`,
    GALLERYVIDEOS: 'gallery-videos',
    GALLERYVIDEOSTHUMB: `gallery-videos/thumbs`,
    GROUPSPHOTOS: `groups`,
    TIPSPHOTOS: 'tips',
    REFERRALAWARDSPHOTOS: 'referrals/awards'
};

export const DEFAULTIMAGES = {
    SPECIES: `default_specie.jpeg`,
    CATCHES: `default.png`,
    BAITS: `default-120x120.png`,
    LURES: `default-120x120.png`,
    GROUPS: `default.png`
}

export const COLLATION = {
    ES: {locale: `es`, strength: 1},
};

export const REDIS_KEYS = {
    MOON_PHASE: "MOON_PHASE"
}

export const LEVELS = {
    FIRST: 1,
    LEGEND: 12
}

export const DATE_FIELDS = [
    'createdAt',
    'updateAt',
    'date'
];

export const OBJECTID_FIELDS = [
    '_id'
];

export const COUNTRIES = {
    SPAIN: "hqkHEqkQ5c6E6Wxjm",
    USA: "6zv2ainyXTewfa7b9"
}

export const STICKERS_COLORS = {
    PURPLE: { label: "purple" },
    YELLOW: { label: "yellow" },
    GREEN: { label: "green" },
    BLUE: { label: "blue" },
}