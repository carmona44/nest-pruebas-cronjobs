import { Module } from '@nestjs/common';
import { PruebasModule } from 'src/pruebas/pruebas.module';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';

@Module({
  imports: [PruebasModule],
  providers: [TasksService],
  controllers: [TasksController]
})
export class TasksModule {}
